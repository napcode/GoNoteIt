package eu.napcode.gonoteit.repository.calendar

import android.arch.lifecycle.LiveData
import eu.napcode.gonoteit.data.calendar.CalendarResult
import eu.napcode.gonoteit.data.notes.results.NotesResult
import eu.napcode.gonoteit.model.note.NoteModel

interface CalendarRepository {
    fun getTodayEvents(): CalendarResult

    fun getTomorrowEvents(): NotesResult

    fun getWeekEvents() : CalendarResult

    fun getEventsFromToday(multiplier : Int = 1) : CalendarResult

    fun getLastEvent() : LiveData<NoteModel>
}