package eu.napcode.gonoteit.repository.calendar

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import eu.napcode.gonoteit.data.calendar.CalendarLocal
import eu.napcode.gonoteit.data.calendar.CalendarResult
import eu.napcode.gonoteit.data.notes.results.NotesResult
import eu.napcode.gonoteit.model.note.NoteModel
import eu.napcode.gonoteit.repository.Resource
import eu.napcode.gonoteit.repository.notes.NotesRepository
import javax.inject.Inject

class CalendarRepositoryImpl @Inject
constructor(private val calendarLocal: CalendarLocal, private val notesRepository: NotesRepository) : CalendarRepository {

    internal var resource = MutableLiveData<Resource<*>>()

    override fun getTodayEvents(): CalendarResult {
        return CalendarResult(calendarLocal.getTodayEvents(), resource)
    }

    override fun getTomorrowEvents(): NotesResult {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getWeekEvents(): CalendarResult {
        var notesResult = notesRepository.getNotes()

        return CalendarResult(calendarLocal.getCurrentWeek(), notesResult.resource)
    }

    override fun getEventsFromToday(multiplier: Int): CalendarResult {
        var notesResult = notesRepository.getNotes()
        val numberOfEvents = 10

        return CalendarResult(calendarLocal.getEventsFromToday(multiplier * numberOfEvents), notesResult.resource)
    }

    override fun getLastEvent(): LiveData<NoteModel> {
        return calendarLocal.getLastEvent()
    }
}