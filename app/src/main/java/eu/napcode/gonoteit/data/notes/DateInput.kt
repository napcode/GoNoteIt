package eu.napcode.gonoteit.data.notes

public fun getDateInput(date: Long?) : Int {
    if (date == null) {
        return -1
    }

    return date.toInt()
}