package eu.napcode.gonoteit.ui.calendar

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.support.annotation.LayoutRes
import android.support.v4.content.ContextCompat
import android.support.v4.util.Pair
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import eu.napcode.gonoteit.R
import eu.napcode.gonoteit.model.note.NoteModel
import eu.napcode.gonoteit.utils.*
import kotlinx.android.synthetic.main.item_calendar_event.view.*
import kotlinx.android.synthetic.main.item_calendar_event.view.divider
import kotlinx.android.synthetic.main.item_calendar_footer.view.*
import kotlinx.android.synthetic.main.item_date.view.*
import java.util.*

class CalendarAdapter(var context: Context, var listener: CalendarEventListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var calendarElements: List<CalendarAdapterElement> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    var reachedLast = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            LAYOUT_FOOTER -> FooterViewHolder(parent.inflate(R.layout.item_calendar_footer))
            LAYOUT_DATE -> DateViewHolder(parent.inflate(R.layout.item_date))
            else -> EventViewHolder(parent.inflate(R.layout.item_calendar_event))
        }
    }

    override fun getItemCount() = calendarElements.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        return when(holder.itemViewType) {
            LAYOUT_FOOTER -> (holder as FooterViewHolder).bind(position)
            LAYOUT_DATE -> (holder as DateViewHolder).bind(position)
            else -> (holder as EventViewHolder).bind(position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position == calendarElements.size -> LAYOUT_FOOTER
            calendarElements[position].isDate -> LAYOUT_DATE
            else -> LAYOUT_EVENT
        }
    }

    inner class DateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val date = dateFormat.format(calendarElements[position].date!!.time)
            val today = getTodayCalendar()
            val tomorrow = getTomorrowCalendar()

            when {
                isSameDate(calendarElements[position].date!!, today) -> displayDateForToday(date, itemView)
                isSameDate(calendarElements[position].date!!, tomorrow) -> displayDateForTomorrow(date, itemView)
                else -> itemView.headerTextView.text = date
            }
        }
    }

    fun displayDateForToday(date: String, itemView: View) {
        val todayString = context.getString(R.string.today)
        val displayText = "$todayString  $date"
        itemView.headerTextView.text = displayText

        itemView.headerTextView.background = ColorDrawable(ContextCompat.getColor(context, R.color.colorAccent))
    }

    fun displayDateForTomorrow(date: String, itemView: View) {
        val tomorrowString = context.getString(R.string.tomorrow)
        val displayText = "$tomorrowString  $date"
        itemView.headerTextView.text = displayText

        itemView.headerTextView.background = ColorDrawable(ContextCompat.getColor(context, R.color.colorPrimaryDark))
    }

    inner class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {

            if (calendarElements[position].note == null) {
                displayNoEvents(calendarElements[position-1].date!!, itemView)

                return
            }

            if (calendarElements[position - 1].isDate) {
                itemView.divider.visibility = GONE
            } else {
                itemView.divider.visibility = VISIBLE
            }

            val note = calendarElements[position].note!!
            displayEvent(itemView, note)

            displayBiggerPaddingIfLast(itemView, position)
        }
    }

    private fun displayNoEvents(calendar: Calendar, itemView: View) {

        if (isSameDate(calendar, getTodayCalendar())) {
            displayNoEventsToday(itemView)
        } else {
            displayNoEventsTomorrow(itemView)
        }
    }

    private fun displayNoEventsToday(itemView: View) {
        setNoEventsViews(itemView)
        itemView.noEvents.text = context.getString(R.string.no_events_today)
    }

    private fun setNoEventsViews(itemView: View) {
        itemView.eventTitleTextView.visibility = GONE
        itemView.eventHourTextView.visibility = GONE
        itemView.divider.visibility = GONE
        itemView.noEvents.visibility = VISIBLE
    }

    private fun displayNoEventsTomorrow(itemView: View) {
        setNoEventsViews(itemView)
        itemView.noEvents.text = context.getString(R.string.no_events_tomorrow)
    }

    private fun displayEvent(itemView: View, note: NoteModel) {
        itemView.eventTitleTextView.visibility = VISIBLE
        itemView.eventHourTextView.visibility = VISIBLE
        itemView.noEvents.visibility = GONE

        itemView.eventTitleTextView.text = note.title
        var calendar = Calendar.getInstance()
        calendar.timeInMillis = note.date!!
        itemView.eventHourTextView.text = timeFormat.format(calendar.time)
        itemView.eventHourTextView.setPadding(0, 0, 0, 0)
        itemView.eventTitleTextView.setPadding(0, 0, 0, 0)

        itemView.eventContainer.setOnClickListener {
            listener.onClickNote(note, getSharedElementPairs(itemView))
        }
    }

    private fun displayBiggerPaddingIfLast(itemView: View, position: Int) {
        if (position == calendarElements.size - 1
                || calendarElements[position + 1].isDate) {
            val padding = context.resources.getDimension(R.dimen.base_margin)
            itemView.eventHourTextView.setPadding(0, 0, 0, padding.toInt())
            itemView.eventTitleTextView.setPadding(0, 0, 0, padding.toInt())
        }
    }

    private fun getSharedElementPairs(itemView: View): Pair<View, String> {
        return Pair(itemView.eventTitleTextView, context.getString(R.string.transition_note_title))
    }

    private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }

    inner class FooterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            itemView.moreButton.isEnabled = !reachedLast
            itemView.moreButton.setOnClickListener { listener.onLoadMoreClicked() }
        }
    }

    companion object {
        const val LAYOUT_DATE = 100
        const val LAYOUT_EVENT = 101
        const val LAYOUT_FOOTER = 102
    }

    interface CalendarEventListener {
        fun onClickNote(noteModel: NoteModel, vararg sharedElementPairs: Pair<View, String>)

        fun onLoadMoreClicked()
    }
}