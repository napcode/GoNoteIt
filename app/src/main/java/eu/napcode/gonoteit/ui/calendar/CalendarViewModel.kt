package eu.napcode.gonoteit.ui.calendar

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import eu.napcode.gonoteit.data.calendar.CalendarResult
import eu.napcode.gonoteit.model.note.NoteModel
import eu.napcode.gonoteit.repository.calendar.CalendarRepository
import javax.inject.Inject

class CalendarViewModel @Inject
constructor(private val calendarRepository: CalendarRepository) : ViewModel() {

    var eventsMultiplier = 1
    lateinit var calendarResult: CalendarResult
    val lastEvent : LiveData<NoteModel> = calendarRepository.getLastEvent()

    fun getEventsFromToday() {
        eventsMultiplier = 1
        calendarResult = calendarRepository.getEventsFromToday()
    }

    fun getMoreEvents(){
        eventsMultiplier++
        calendarResult = calendarRepository.getEventsFromToday(eventsMultiplier)
    }

    fun didLoadAll() : Boolean {
        return lastEvent.value?.equals(calendarResult.notes.value?.last()) ?: false
    }
}
