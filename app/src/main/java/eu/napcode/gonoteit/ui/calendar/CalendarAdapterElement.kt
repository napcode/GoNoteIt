package eu.napcode.gonoteit.ui.calendar

import eu.napcode.gonoteit.model.note.NoteModel
import java.util.*

class CalendarAdapterElement(val isDate: Boolean,
                             val note: NoteModel?,
                             val date: Calendar?)