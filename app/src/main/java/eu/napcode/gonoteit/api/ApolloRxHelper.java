package eu.napcode.gonoteit.api;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloQueryWatcher;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.rx2.Rx2Apollo;

import io.reactivex.Observable;

/**Wrapper to Rx2Apollo static methods to improve testability*/
public class ApolloRxHelper {

    public ApolloRxHelper() {
    }

    public <T> Observable<Response<T>> from(final ApolloQueryWatcher<T> watcher) {
        return Rx2Apollo.from(watcher);
    }

    public <T> Observable<Response<T>> from(final ApolloCall<T> call) {
        return Rx2Apollo.from(call);
    }
}
