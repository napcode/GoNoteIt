package eu.napcode.gonoteit.api

import com.apollographql.apollo.response.CustomTypeAdapter
import com.apollographql.apollo.response.CustomTypeValue

class UUIDAdapter : CustomTypeAdapter<String> {

    override fun decode(value: CustomTypeValue<*>): String {
        return value.value.toString()
    }

    override fun encode(value: String): CustomTypeValue<*> {
        return CustomTypeValue.fromRawValue(value)
    }
}
