package eu.napcode.gonoteit.api

import com.apollographql.apollo.response.CustomTypeAdapter
import com.apollographql.apollo.response.CustomTypeValue

class NoteAdapter : CustomTypeAdapter<Note> {

    override fun decode(value: CustomTypeValue<*>): Note {
        return Note(value.value.toString())
    }

    override fun encode(note: Note): CustomTypeValue<*> {
        return CustomTypeValue.fromRawValue(note.noteDataString)
    }
}
