package eu.napcode.gonoteit.api

import java.lang.reflect.InvocationTargetException

import eu.napcode.gonoteit.type.Access
import eu.napcode.gonoteit.type.Type

class ApiEntity @Throws(NoSuchMethodException::class, InvocationTargetException::class, IllegalAccessException::class)
constructor(o: Any) {

    var id: Long? = null
        internal set
    var updatedAt: Long? = null
        internal set
    var type: Type
        internal set
    var readAccess: Access
        internal set
    var writeAccess: Access
        internal set
    var date: Long? = null
        internal set

    init {
        id = (o.javaClass.getMethod(ID_METHOD).invoke(o) as Int).toLong()
        updatedAt = (o.javaClass.getMethod(UPDATED_AT).invoke(o) as Int).toLong()
        type = o.javaClass.getMethod(TYPE_METHOD).invoke(o) as Type
        readAccess = o.javaClass.getMethod(READ_PERMS_METHOD).invoke(o) as Access
        writeAccess = o.javaClass.getMethod(WRITE_PERMS_METHOD).invoke(o) as Access
        date = (o.javaClass.getMethod(DATE_METHOD).invoke(o) as Int?)?.toLong()
    }

    companion object {

        private val ID_METHOD = "id"
        private val TYPE_METHOD = "type"
        private val UPDATED_AT = "updatedAt"
        private val READ_PERMS_METHOD = "readAccess"
        private val WRITE_PERMS_METHOD = "writeAccess"
        private val DATE_METHOD = "date"
    }
}
